#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "minako.h"

int currentToken;
int nextToken;


int isToken(int token) {
	return (token == currentToken);
}

void eat() {
  currentToken = nextToken;
  nextToken = yylex();
}

void isTokenAndEat(int token){
  if(!isToken(token)){
    puts("Invalid Token Error\n");
    exit(-1);
  }
  currentToken = nextToken;
  nextToken = yylex();
}

typedef void MetaSymbol(void);
MetaSymbol program, functiondefinition, type, statementlist, block, statement, ifstatement, printF, returnstatement, functioncall, statassignment, assignment, expr, simpexpr, term, factor ;

void program() {
  while(isToken(265) || isToken(268) || isToken(271) || isToken(274)){
    functiondefinition();
  }
  isTokenAndEat(-1);
}

void functiondefinition(){
  type();
  isTokenAndEat(279);
  isTokenAndEat(40);
  isTokenAndEat(41);
  isTokenAndEat(123);
  statementlist();
  isTokenAndEat(125);
}

void statementlist(){
  while(isToken(123) || isToken(279) || isToken(270) || isToken(272) || isToken(273)){
    block();
  }
}

void block(){
  if (isToken(123)){
    eat();
    statementlist();
    isTokenAndEat(125);
  }
  else{
    statement();
  }
}

void statement(){
  if(isToken(270)){
    ifstatement();
  }
  else if(isToken(272)){
    printF();
    isTokenAndEat(59);
  }
  else if(isToken(273)){
    returnstatement();
    isTokenAndEat(59);
  }
  else if(isToken(279)){
    if(nextToken == 40){
      functioncall();
      isTokenAndEat(59);
    }
    else if(nextToken == 61){
      statassignment();
      isTokenAndEat(59);
    }
  }
}

void ifstatement(){
  isTokenAndEat(270);
  isTokenAndEat(40);
  assignment();
  isTokenAndEat(41);
  block();
}

void assignment(){
  if(isToken(45) || isToken(276) || isToken(277) || isToken(278) || isToken(40)){
    expr();
  }

  if(isToken(279)){
    if(nextToken == 61){
      eat();
      eat();
      assignment();
    }
    else{
      expr();
    }
  }
}

void returnstatement(){
  isTokenAndEat(273);
  if(isToken(45) || isToken(276) || isToken(277) || isToken(278) || isToken(40) || isToken(279)){
    assignment();
  }
}

void printF(){
  isTokenAndEat(272);
  isTokenAndEat(40);
  assignment();
  isTokenAndEat(41);
}

void type(){
  if(isToken(265) || isToken(268) || isToken(271) || isToken(274)){
    eat();
  }
}

void statassignment(){
  isTokenAndEat(279);
  isTokenAndEat(61);
  assignment();
}

void expr(){
  simpexpr();
  if(isToken(259) || isToken(260) || isToken(261) || isToken(262) || isToken(263) || isToken(264)){
    eat();
    simpexpr();
  }
}

void simpexpr(){
  if(isToken(45)){
    eat();
  }
  term();
  while(isToken(43) || isToken(45) || isToken(258)){
    eat();
    term();
  }
}

void term(){
  factor();
  while(isToken(47) || isToken(42) || isToken(257)){
    eat();
    factor();
  }

}

void factor(){
  if(isToken(276) || isToken(277) || isToken(278)){
    eat();
  }
  if(isToken(40)){
    eat();
    assignment();
    isTokenAndEat(41);
  }
  if(isToken(279)){
    if(nextToken == 40){
      functioncall();
    }
    else{
      eat();
    }
  }
}

void functioncall(){
  isTokenAndEat(279);
  isTokenAndEat(40);
  isTokenAndEat(41);
}

int main(int argc, const char *argv[]) {
	if (argc >= 1){
		yyin = fopen(argv[1], "r");
	if(yyin == NULL){
	  printf("Die Datei kann nicht geoeffnet werden.\n");
	  exit(-1);
}
 	}
	else{
  }
  currentToken = yylex();
  nextToken = yylex();
  program();
	return 0;
}
